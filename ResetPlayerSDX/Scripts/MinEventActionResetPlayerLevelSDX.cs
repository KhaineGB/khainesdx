using UnityEngine;
using System.Collections.Generic;
using System.Xml; 

public class MinEventActionResetPlayerLevelSDX : MinEventActionRemoveBuff
{
	int xpToLevel = 1;
	//<triggered_effect trigger="onSelfBuffStart" action="ResetPlayerLevelSDX, Mods" target="self" value="10000"/> value is the default xp to next level in progression
    public override void Execute(MinEventParams _params)
    {
        for (int i = 0; i < this.targets.Count; i++)
        {
            EntityPlayerLocal entity = this.targets[i] as EntityPlayerLocal;
            if (entity != null)
            {
                if (entity.Progression.Level <= Progression.MaxLevel)
                {
                    entity.Progression.Level = 1; //this sets player level to 1
                    entity.Progression.ExpToNextLevel = xpToLevel; //this sets player xp to a user defined value
                }
				
            }
        }
    }
	
    public override bool ParseXmlAttribute(XmlAttribute _attribute)
    {
        bool flag = base.ParseXmlAttribute(_attribute);
        if (!flag)
        {
            string name = _attribute.Name;
            if (name != null)
            {
                if (name == "value" )
                {
                    int.TryParse(_attribute.Value, out xpToLevel);
                    return true;
                }
            }
        }
        return flag;
    }
}