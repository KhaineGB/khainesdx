using UnityEngine;
using System.Collections.Generic;
using System.Xml; 

public class MinEventActionResetPlayerSkillsSDX : MinEventActionRemoveBuff
{
	int skillPoints = 0;
	//<triggered_effect trigger="onSelfBuffStart" action="ResetPlayerSkillsSDX, Mods" target="self" value="0"/> value can be whatever the user wants but 0 is default
    public override void Execute(MinEventParams _params)
    {
        for (int k = 0; k < this.targets.Count; k++)
        {
            EntityPlayer entity = this.targets[k] as EntityPlayer;
            if (entity != null)
                entity.Progression.SkillPoints = 0; //this removes all skill points
        }
    }
	
    public override bool ParseXmlAttribute(XmlAttribute _attribute)
    {
        bool flag = base.ParseXmlAttribute(_attribute);
        if (!flag)
        {
            string name = _attribute.Name;
            if (name != null)
            {
                if (name == "value" )
                {
                    int.TryParse(_attribute.Value, out skillPoints);
                    return true;
                }
            }
        }
        return flag;
    }
}