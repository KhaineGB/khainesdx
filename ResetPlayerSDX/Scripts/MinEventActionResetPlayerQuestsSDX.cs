using UnityEngine;
using System.Collections.Generic; 

public class MinEventActionResetPlayerQuestsSDX : MinEventActionRemoveBuff
{
	//<triggered_effect trigger="onSelfBuffStart" action="ResetPlayerQuestsSDX, Mods" target="self"  /> 
	string strQuestOnRespawn = "quest_PlayerRespawn";

    public override void Execute(MinEventParams _params)
    {
		for (int j = 0; j < this.targets.Count; j++)
		{
			EntityPlayerLocal Playerentity = this.targets[j] as EntityPlayerLocal;
			
			//This block of code clears the active quest
			for (int l = Playerentity.QuestJournal.quests.Count - 1; l >= 0; l--)
			{
				if (Playerentity.QuestJournal.quests[l].CurrentState == Quest.QuestState.InProgress || Playerentity.QuestJournal.quests[l].CurrentState == Quest.QuestState.ReadyForTurnIn)
				{
					Quest quest = Playerentity.QuestJournal.quests[l];

					Playerentity.QuestJournal.RemoveQuest(quest);
				}
			}
			
			//This clears the players quest journal
			if (Playerentity != null)
			{
				Playerentity.QuestJournal.quests.Clear();  
				Playerentity.QuestJournal.sharedQuestEntries.Clear();  			
			}
			
			//This clears the players trader faction for quest tiers
			if (Playerentity != null)
			{
				Playerentity.QuestJournal.QuestFactionPoints.Clear();  
			}
			
			//This gives the player the 'Find the Trader' quest instead of the full tutorial
            if (Playerentity != null)
            {
                if (string.IsNullOrEmpty(this.strQuestOnRespawn))
                    continue;

                Quest myQuest = QuestClass.CreateQuest(this.strQuestOnRespawn);
                myQuest.QuestGiverID = -1;
                Playerentity.QuestJournal.AddQuest(myQuest);
                
            }
		}
    }
}