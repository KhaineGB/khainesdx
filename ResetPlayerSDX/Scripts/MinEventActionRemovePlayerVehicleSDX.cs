using UnityEngine;
using System.Collections.Generic; 

public class MinEventActionRemovePlayerVehicleSDX : MinEventActionRemoveBuff
{
	//<triggered_effect trigger="onSelfBuffStart" action="RemovePlayerVehicleSDX, Mods" target="self"  /> 
    public override void Execute(MinEventParams _params)
    {
		for (int j = 0; j < this.targets.Count; j++)
		{
			//EntityPlayerLocal Playerentity = this.targets[j] as EntityPlayerLocal;
			
			for (int i = 0; i < GameManager.Instance.World.Entities.list.Count; i++)
			{
				EntityVehicle entityVehicle = GameManager.Instance.World.Entities.list[i] as EntityVehicle;
				//int id = __instance.entityId;
													
				//if (entityVehicle != null && entityVehicle.HasOwnedEntity(id))
				if (entityVehicle != null)
				{
					entityVehicle.Kill();
				}
			}
		}
    }
}