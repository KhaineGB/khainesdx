using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;

public class MinEventActionRemoveLandClaimsSDX : MinEventActionRemoveBuff
{
	//<triggered_effect trigger="onSelfDied" action="RemoveLandClaimsSDX, Mods" target="self"  /> 
    public override void Execute(MinEventParams _params)
    {
		var playerId = GameManager.Instance.World.GetPrimaryPlayerId();
		var positions = GameManager.Instance.persistentPlayers.m_lpBlockMap.Keys.ToList();
            
		foreach (var pos in positions)
		{
			var playerInfo = GameManager.Instance.persistentPlayers.m_lpBlockMap[pos];
			if (playerInfo.EntityId == playerId)
			{
				GameManager.Instance.persistentPlayers.RemoveLandProtectionBlock(pos);
			}
		}
    }
}