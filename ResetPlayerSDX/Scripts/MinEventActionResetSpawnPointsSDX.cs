using UnityEngine;
using System.Collections.Generic; 

public class MinEventActionResetSpawnPointsSDX : MinEventActionRemoveBuff
{
	//<triggered_effect trigger="onSelfDied" action="ResetSpawnPointsSDX, Mods" target="self"  /> 
    public override void Execute(MinEventParams _params)
    {
		for (int j = 0; j < this.targets.Count; j++)
		{
			EntityPlayerLocal Playerentity = this.targets[j] as EntityPlayerLocal;
			
			Playerentity.SpawnPoints.Clear();
			Playerentity.selectedSpawnPointKey = -1L;
		}
    }
}